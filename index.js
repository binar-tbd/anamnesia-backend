const express = require("express");

const app = express();
const port = 8000;
app.use(express.static("public"));

const cors = require("cors");

app.use(
  cors({
    origin: "http://localhost:3000",
    credentials: true,
  }),
);

const logger = require("morgan");

app.use(logger("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const router = require("./routes/api");

app.use(router);

app.listen(port);
