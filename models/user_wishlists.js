const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class userWishlists extends Model {
    static associate(models) {
      models.user_wishlists.belongsTo(models.news, {
        foreignKey: "news_id",
        as: "userWishlists_news_id",
      });

      models.user_wishlists.belongsTo(models.users, {
        foreignKey: "users_id",
        as: "userWishlists_users_id",
      });
    }

    static addNewsWishList = async function ({ newsId, usersId }) {
      await this.create({ news_id: newsId, users_id: usersId });
      const data = this.count("id", {
          where: {
              news_id: newsId,
          },
      });

      return data;
    };
  }
  userWishlists.init(
      {
          users_id: {
              type: DataTypes.INTEGER,
              field: "users_id",
          },
          news_id: {
              type: DataTypes.INTEGER,
              field: "news_id",
          },
      },
      {
          sequelize,
          modelName: "user_wishlists",
      },
  );
  return userWishlists;
};
