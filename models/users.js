const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    static associate(models) {
      models.users.hasMany(models.news, {
        foreignKey: "id",
        as: "users_news_id",
      });
      models.users.hasMany(models.user_likes, {
        foreignKey: "id",
        as: "users_userLike_id",
      });
      models.users.hasMany(models.user_wishlists, {
        foreignKey: "id",
        as: "users_userWishlists_id",
      });
    }
  }

  users.init(
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      full_name: DataTypes.STRING,
      gender: DataTypes.STRING,
      address: DataTypes.STRING,
      phone: DataTypes.STRING,
      role: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "users",
    },
  );
  return users;
};
