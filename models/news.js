const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class news extends Model {
        static associate(models) {
            models.news.belongsTo(models.categories, {
                foreignKey: "categories_id",
                as: "news_categories_id",
            });

            models.news.belongsTo(models.users, {
                foreignKey: "users_id",
                as: "news_users_id",
            });

            models.news.hasMany(models.user_likes, {
                foreignKey: "id",
                as: "news_userLikes_id",
            });

            models.news.hasMany(models.user_wishlists, {
                foreignKey: "id",
                as: "news_userWishlists_id",
            });
        }

        static detailNews = function (id) {
            const data = this.findOne({
                where: { id },
                include: [
                    {
                        model: sequelize.models.categories,
                        as: "news_categories_id",
                    },
                    { model: sequelize.models.users, as: "news_users_id" },
                ],
            });

            return data;
        };
    }
    news.init(
        {
            headline: DataTypes.STRING,
            body: DataTypes.TEXT,
            image: DataTypes.STRING,
            categories_id: {
                type: DataTypes.INTEGER,
                field: "categories_id",
            },
            status: DataTypes.STRING,
            type_input: DataTypes.STRING,
            link: DataTypes.STRING,
            viewer: DataTypes.INTEGER,
            like: DataTypes.INTEGER,
            users_id: {
                type: DataTypes.INTEGER,
                field: "users_id",
            },
        },
        {
            sequelize,
            modelName: "news",
        },
    );
    return news;
};
