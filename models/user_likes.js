const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
    class user_likes extends Model {
        static associate(models) {
            models.user_likes.belongsTo(models.news, {
                foreignKey: "news_id",
                as: "userLikes_news_id",
            });

            models.user_likes.belongsTo(models.users, {
                foreignKey: "users_id",
                as: "userLikes_users_id",
            });
        }

        static getLikeNews = function ({ newsId }) {
            const data = this.count("id", {
                where: {
                    news_id: newsId,
                },
            });

            return data;
        };

        static getUserLikeNews = function ({ usersId }) {
            const data = this.findOne({
                where: {
                    users_id: usersId,
                },
            });

            return data;
        };

        static likeNews = async function ({ newsId, usersId }) {
            await this.create({ news_id: newsId, users_id: usersId });
            const data = this.count("id", {
                where: {
                    news_id: newsId,
                },
            });

            return data;
        };

        static unlikeNews = async function ({ newsId, usersId }) {
            await this.destroy({
                where: { users_id: usersId, news_id: newsId },
            });
            const data = this.count("id", {
                where: {
                    news_id: newsId,
                },
            });

            return data;
        };
    }
    user_likes.init(
        {
            users_id: {
                type: DataTypes.INTEGER,
                field: "users_id",
            },
            news_id: {
                type: DataTypes.INTEGER,
                field: "news_id",
            },
        },
        {
            sequelize,
            modelName: "user_likes",
        },
    );
    return user_likes;
};
