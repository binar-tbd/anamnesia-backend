const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class categories extends Model {
    static associate(models) {
      models.categories.belongsTo(models.news, {
        foreignKey: "id",
        as: "categories_news_id",
      });
    }
  }
  categories.init(
    {
      category_name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "categories",
    },
  );
  return categories;
};
