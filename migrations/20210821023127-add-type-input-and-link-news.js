module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn("news", "type_input", Sequelize.STRING);
    queryInterface.addColumn("news", "link", Sequelize.STRING);
  },

  down: async (queryInterface) => {
    queryInterface.removeColumn("news", "type_input");
    queryInterface.removeColumn("news", "link");
  },
};
