module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("user_wishlists", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      users_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "users",
          key: "id",
          onUpdate: "cascade",
          onDelete: "cascade",
        },
      },
      news_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "news",
          key: "id",
          onUpdate: "cascade",
          onDelete: "cascade",
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface) => {
    await queryInterface.dropTable("user_wishlists");
  },
};
