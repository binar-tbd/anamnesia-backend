const express = require("express");

const router = express.Router();
const landingpageController = require("../../../controllers/api/landingpage/landingpagecontroller");

router.get("/listnews", landingpageController.listNewsAction);
router.post("/detailnews", landingpageController.detailNewsAction);

module.exports = router;
