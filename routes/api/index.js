const express = require("express");

const router = express.Router();
const apiNewsRouter = require("./landingpage/landingpage");
const apiDetailNewsRouter = require("./detaillandingpage/detaillandingpage");
const apiWishListNewsRouter = require("./wishlistlandingpage/wishlistlandingpage");

router.use("/api/landingpage", apiNewsRouter);
router.use("/api/detaillandingpage", apiDetailNewsRouter);
router.use("/api/wishlistpage", apiWishListNewsRouter);

module.exports = router;
