const express = require("express");

const router = express.Router();
const wishlistpageController = require("../../../controllers/api/wishlistpage/wishlistpagecontroller");

router.get("/listnewswishlist", wishlistpageController.getAllWishList);
router.post("/addnewswishlist", wishlistpageController.addWishlist);
router.delete("/deletenewswishlist", wishlistpageController.deleteWishList);

module.exports = router;
