const express = require("express");

const router = express.Router();
const detailLandingPageController = require("../../../controllers/api/detaillandingpage/detailLandingPageController");

router.post("/getLikeNews", detailLandingPageController.getLikeNewsAction);
router.post("/getUserLikeNews", detailLandingPageController.getUserLikeNewsAction);
router.post("/likeNews", detailLandingPageController.likeNewsAction);
router.post("/unlike", detailLandingPageController.unlikeNewsAction);

module.exports = router;
