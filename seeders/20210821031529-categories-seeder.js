module.exports = {
  up: async (queryInterface) => queryInterface.bulkInsert(
    "categories",
    [
      {
        category_name: "Entertaiment",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        category_name: "Otomotif",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        category_name: "Sport",
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      {
        category_name: "Sepakbola",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        category_name: "Teknologi",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ],
    {},
  ),

  down: async (queryInterface) => queryInterface.bulkDelete("categories", null, {}),
};
