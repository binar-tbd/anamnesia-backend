const { user_likes } = require("../../../models");

const getLikeNewsAction = function (req, res, next) {
    user_likes
        .getLikeNews(req.body)
        .then((totalLike) => {
            res.json({
                totalLike,
            });
        })
        .catch((err) => next(err));
};

const getUserLikeNewsAction = function (req, res, next) {
    user_likes
        .getUserLikeNews(req.body)
        .then((userLike) => {
            res.json({
                userLike,
            });
        })
        .catch((err) => next(err));
};

const likeNewsAction = function (req, res, next) {
    user_likes
        .likeNews(req.body)
        .then((totalLike) => {
            res.json({
                totalLike,
            });
        })
        .catch((err) => next(err));
};

const unlikeNewsAction = (req, res, next) => {
    user_likes
        .unlikeNews(req.body)
        .then((totalLike) => {
            res.json({
                totalLike,
            });
        })
        .catch((err) => next(err));;
};

module.exports = {
    getLikeNewsAction,
    getUserLikeNewsAction,
    likeNewsAction,
    unlikeNewsAction,
};
