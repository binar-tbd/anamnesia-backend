const { categories, news, users } = require("../../../models");

const listNewsAction = function (req, res, next) {
    news.findAll({
        include: [
            { model: categories, as: "news_categories_id" },
            { model: users, as: "news_users_id" },
        ],
    })
        .then((dataNews) => {
            res.json({ dataNews });
        })
        .catch((err) => next(err));
};

const detailNewsAction = function (req, res, next) {
    news.detailNews(req.body.id)
        .then((dataNews) => {
            res.json({
                dataNews,
            });
        })
        .catch((err) => next(err));
};

module.exports = {
    listNewsAction,
    detailNewsAction,
};
