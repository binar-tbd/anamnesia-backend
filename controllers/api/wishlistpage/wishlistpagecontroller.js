const { userWishlists } = require("../../../models");

const getAllWishList = function (req, res, next) {
  userWishlists
    .findAll()
    .then((dataUserWishlists) => {
      res.json({
        dataUserWishlists,
      });
    })
    .catch((err) => next(err));
};

const addWishlist = function (req, res, next) {
  userWishlists
    .addNewsWishList(req.body)
    .then((data) => {
      res.json({
        data,
      });
    })
    .catch((err) => next(err));
};

const deleteWishList = function (req) {
  userWishlists
    .destroy({
      where: { id: req.params.id },
    })
    .then((res) => {
      res.json({
          status: "success",
      });
    });
};

module.exports = {
  getAllWishList,
  addWishlist,
  deleteWishList,
};
